﻿using UnityEngine;
using System.Collections;

public class DestroyByBoundary : MonoBehaviour {
    public float lifeTime = 2.0f;

    void Awake ()
    {
        Destroy(gameObject, lifeTime);
    }
}
