﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {

    public float speed;
    float moveVelocity;
    public GameObject bulletPrefab;



    void Update()
    {


        moveVelocity = 0;

        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
        {
            moveVelocity = - -speed;    
        }

        if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
        {
            moveVelocity = -+speed;
        }

        GetComponent<Rigidbody2D>().velocity = new Vector2(moveVelocity, GetComponent<Rigidbody2D>().velocity.y);
    }
}
