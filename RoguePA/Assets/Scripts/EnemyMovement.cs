﻿using UnityEngine;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{

    public GameObject Laser;

    public float leftRightSpeed;
    bool WeShouldGoRight = true;
    Vector3 dropAmount = new Vector3(0, -1, 0);
    public Transform myTransform;
    float boundaryLeft = -6.6f;
    float boundaryRight = 6.8f;

    void Update()
    {


        if ((transform.position.x >= boundaryRight) || (transform.position.x <= boundaryLeft))
        {
            WeShouldGoRight = !WeShouldGoRight;
            DropALevel();
        }

        Move(WeShouldGoRight);
    }


    void OnTriggerEnter(Collider collider)
    {


        if (collider.name == "Laser(Clone)")
        {

            Destroy(gameObject);

        }
    }


    void Move(bool moveRight)
    {
        float movementX = moveRight ? leftRightSpeed : -leftRightSpeed;
        transform.Translate(new Vector3(movementX * Time.deltaTime, 0, 0));
    }

    void DropALevel()
    {
        transform.Translate(dropAmount);
    }
}