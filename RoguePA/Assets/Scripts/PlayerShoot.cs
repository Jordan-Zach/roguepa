﻿using UnityEngine;
using System.Collections;

public class PlayerShoot : MonoBehaviour {


    public GameObject Laser;
    public Transform shotSpawn;
    public float fireRate;

    private float nextFire;

	
	void Update () {

        {
            if (Input.GetButton("Fire1") && Time.time > nextFire)
            {
                nextFire = Time.time + fireRate;
                Instantiate(Laser, shotSpawn.position, shotSpawn.rotation);
            }
        }
	}
}
